<?php
require_once get_template_directory() . '/libs/class-wp-bootstrap-navwalker.php';

function my_acf_json_save_point($path)
{
    return get_stylesheet_directory() . '/acf-json';
}

function origami_custom_post(){
    register_post_type('colaborador', array(
        'labels' => array(
            'name' => __('Colaboradores'),
            'singular_name' => __('Colaborador')
        ),
        'public' => true,
        'has_archive' => false,
        'show_in_rest' => true,
        'supports' => array('title', 'thumbnail', 'excerpt'),
        'menu_icon'   => 'dashicons-id-alt'
    ));

    register_post_type('cliente', array(
        'labels' => array(
            'name' => __('Clientes'),
            'singular_name' => __('Cliente')
        ),
        'public' => true,
        'has_archive' => false,
        'show_in_rest' => true,
        'supports' => array('title', 'thumbnail', 'excerpt'),
        'menu_icon'   => 'dashicons-smiley'
    ));

    register_post_type('proyecto', array(
        'labels' => array(
            'name' => __('Proyectos'),
            'singular_name' => __('Proyecto')
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'work'),
        'show_in_rest' => true,
        'supports' => array('title','editor', 'excerpt', 'thumbnail'),
        'menu_icon'   => 'dashicons-buddicons-replies'
    ));

    //servicios
    register_taxonomy('servicios',array('proyecto'), array(
        'hierarchical' => true,
        'labels' => array(
            'name' => __('Servicios'),
            'singular_name' => __('Servicio')
        ),
        'show_in_rest' => true,
        'show_ui' => true
    ));
}

function origami_setup()
{
    add_theme_support('post-thumbnails');
    add_image_size('project-thumbnail', 1080, 1080, true);
}

function origami_styles()
{
    wp_enqueue_style('style', get_template_directory_uri() . '/src/css/style.css', array(), '0.7.64');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('CharlevoixPro', get_template_directory_uri() . '/fonts/CharlevoixPro/stylesheet.css');
    wp_enqueue_style('poppins', '//fonts.googleapis.com/css?family=Poppins:400,500,600&font-display=swap');
}

function origami_menus()
{
    register_nav_menus(array(
        'header-menu' => __('Header Menu', 'apame'),
        'social-menu' => __('Social Menu', 'apame'),
    ));
}

//filters
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

//Hooks
add_action('after_setup_theme', 'origami_setup');
add_action('after_setup_theme', 'origami_menus');
add_action('wp_enqueue_scripts', 'origami_styles');
add_action('init', 'origami_custom_post');
add_filter('acf/settings/show_admin', '__return_false');
