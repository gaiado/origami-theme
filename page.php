<?php get_header(); ?>

<?php 
if (have_posts()):
while (have_posts()): the_post();
?>
<div class="page contacto">
    <?php include_once 'page_header.php'; ?>
    <section class="section-2 container pt-3">
        <?php the_content() ?>
    </section>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>