<?php get_header(); ?>
<?php 
if (have_posts()):
while (have_posts()): the_post();
?>
<div class="work-single">
    <div class="container-fluid">
        <?php the_content(); ?>
    </div>
    <div class="container-fluid">
        <div class="navigator row align-items-center">
            <div class="col">
                <?php 
                $prev_post = get_previous_post();
                if ( isset($prev_post) &&  isset($prev_post->ID))  : 
                ?>
                <a href="<?php echo get_permalink( $prev_post->ID ); ?>">SIGUIENTE</a>
                <?php endif; ?>
                <a class='span' href='/work'>PROYECTOS</a>
                <?php 
                $next_post = get_next_post();
            if ( isset($next_post) &&  isset($next_post->ID))  : 
                ?>
                <a href="<?php echo get_permalink( $next_post->ID ); ?>">ANTERIOR</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <?php include_once('_footer.php'); ?>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>