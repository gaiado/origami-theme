<div class="row footer align-items-center">
    <div class="col text-left">
        origamicreativa <?php echo date('Y'); ?>
    </div>
    <div class="col-6 col-sm-4 col-md-3  text-right">
        <div class="row">
            <div class="col-2 col-xl-1">
                <a target="_blank" href="https://www.linkedin.com/company/origami-boutique-creativa-s-a--de-c-v-/">
                    <i class="fa fa-linkedin"></i>
                </a>
            </div>
            <div class="col-2 col-xl-1">
                <a target="_blank" href="https://www.behance.net/Origamicreativa">
                    <i class="fa fa-behance"></i>
                </a>
            </div>
            <div class="col-2 col-xl-1">
                <a target="_blank" href="https://www.facebook.com/Origamicreativa/">
                    <i class="fa fa-facebook"></i>
                </a>
            </div>
            <div class="col-2 col-xl-1">
                <a target="_blank" href="https://www.instagram.com/origamicreativa/">
                    <i class="fa fa-instagram"></i>
                </a>
            </div>
            <div class="col-2 col-xl-1">
                <a target="_blank" href="https://origamicreativa.wordpress.com/">
                    <i class="fa fa-wordpress" aria-hidden="true"></i>
                </a>
            </div>           
        </div>
    </div>
</div>