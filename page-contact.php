<?php get_header(); ?>
<?php 
if (have_posts()):
while (have_posts()): the_post();
?>
<div id="fullpage" class="contact">
    <div class="section" id="contacto">
        <div class="container-fluid">
            <div class="row no-gutters justify-content-center align-items-center">
                <div class="col-sm-4 mb-4 mb-sm-0 datos">
                    <h2 class="mt-4 mt-sm-0 pt-4 pt-sm-0">
                        ¡HOLA!
                    </h2>
                    <div class="row align-items-center">
                        <div class="col-3">
                            <span class="icon-phone"><i class="fa fa-phone" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-9">
                            <a target="_blank" href="tel:+525563814539"><strong>(+52) 55 6381 4539</strong></a>
                        </div>
                        <div class="col-3">
                            <span class="icon-envelope"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-9">
                            <a target="_blank" href="mailto:hola@origamicreativa.com">
                                <strong>hola</strong>@origamicreativa.com
                            </a>
                        </div>
                        <div class="col-3">
                            <span class="icon-map"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-9">
                            <a target="_blank"
                                href="https://www.google.com/maps/place/Origami+Creativa/@19.4058224,-99.1746183,20z/data=!4m5!3m4!1s0x85d1ff699691ac3d:0xff2696447be2548a!8m2!3d19.4059187!4d-99.1744356?shorturl=1">
                                <div>
                                    <strong>Sultepec No. 25</strong><br>
                                    Col. Hipódromo, CDMX.
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 mb-4 mb-sm-0 pb-4 pb-sm-0">
                    <div class="embed-responsive embed-responsive-1by1">
                        <div class="embed-responsive-item row no-gutters align-items-center justify-content-center form">
                            <div class="col-7">
                                <div><?php the_content(); ?></div>
                                <!--div>
                                    <input type="text" placeholder="NOMBRE">
                                </div>
                                <div>
                                    <input type="text" placeholder="MAIL">
                                </div>
                                <div>
                                    <input type="text" placeholder="TELÉFONO">
                                </div>
                                <div>
                                    <input type="text" placeholder="SERVICIO">
                                </div>
                                <div>
                                    <input type="text" placeholder="MENSAJE">
                                </div>
                                <div class="text-center pt-1 pt-sm-4">
                                    <button class="btn btn btn-light">ENVIAR</button>
                                </div-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-fixed">
        <?php include_once('_footer.php'); ?>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>