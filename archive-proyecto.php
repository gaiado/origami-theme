<?php get_header(); ?>
<?php 
global $wp_query;
$args = array_merge( 
    $wp_query->query_vars, ['posts_per_page' => 100, 'orderby' => 'date' ] 
);
query_posts( $args );
$servicios = array();
if (have_posts()): 
while (have_posts()): the_post();

$servicio = get_the_terms(get_the_ID(),'servicios')[0]; 

if(isset($servicios[$servicio->term_id])){
    $servicio = $servicios[$servicio->term_id];
}else{
    $servicio->posts = array();
}

$servicio->posts[] = get_post();
$servicios[$servicio->term_id] = $servicio;
?>
<?php endwhile; endif; ?>
<div class="work">
    <div class="container-fluid">
        <div class="row no-gutters">
            <?php foreach($servicios as $servicio) :?>
            <div class="col-sm-4 col-6">
                <?php
                    $imagen = get_field('imagen', 'servicios_'.$servicio->term_id);
                    $color = get_field('color', 'servicios_'.$servicio->term_id);
                ?>
                <div id="<?php echo $servicio->slug; ?>" class="embed-responsive embed-responsive-1by1"
                    style="background-image: url(<?php echo $imagen; ?>);">
                </div>
            </div>
            <?php foreach($servicio->posts as $post) :?>

            <div class="col-sm-4 col-6">
                <a href="<?php the_permalink($post->ID); ?>" class="embed-responsive embed-responsive-1by1"
                    style="background-color: <?php echo $color; ?>;background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'project-thumbnail') ?>);">
                    <span><?php echo $post->post_title; ?></span>
                </a>
            </div>

            <?php endforeach; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="footer-bottom">
        <?php include_once('_footer.php'); ?>
    </div>
</div>
<?php get_footer(); ?>