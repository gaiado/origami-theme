<?php
if(!isset($titulo)){
    $titulo =  get_the_title();
}
if(!isset($subtitulo)){
    $subtitulo = get_field('subtitulo');
}

if(!isset($imagen)){
    $imagen = get_the_post_thumbnail_url(get_the_ID(),'full');
}
?>

<section class="section-1 d-flex align-items-center justify-content-center" id="inicio"
    style="background-image: url(<?php echo $imagen ?>)">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-7">
                <h1 class="text-center"><?php echo $titulo ?></h1>
                <p class="text-center">
                    <?php echo $subtitulo ?>
                </p>
            </div>
        </div>
    </div>
</section>