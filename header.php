<!DOCTYPE html>
<html lang="es_mx" ontouchmove>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
  <meta name="author" content="zonakb.com" />
  <?php wp_head(); ?>
</head>

<body class="<?php echo is_admin_bar_showing() ? 'has-admin-bar' : '' ?> overflow" onclick tabIndex=0>
  <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
  <script>
    if ((navigator.userAgent.match(/(iPad)/) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1) )) {
      document.body.className += ' isIpad';
    }else{
      document.body.className += ' isNotIpad';
    }

    if (navigator.platform.match(/iPhone/i)) {
      document.body.className += ' isIphone';
    }else{
      document.body.className += ' isNotIphone';
    }
  </script>
  <nav class="navbar navbar-light bg-light fixed-top">
    <a class="navbar-brand" href="/">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" height="30" class="d-inline-block align-top"
        alt="Origami">
    </a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    </button>

    <div class="collapse navbar-collapse w-100 d-block" id="navbarSupportedContent">
      <div class="row align-items-center">
        <div class="col-3 text-center">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" height="150"
            class="d-inline-block align-top" alt="Origami">
        </div>
        <div class="col-9">
          <div class="row justify-content-center">
            <div class="col-sm-10">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="/">Inicio</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/about">Nosotros</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/work">Proyectos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/contact">Contacto</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>

  <?php include_once('video.php'); ?>