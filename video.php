<?php 
$videos  = scandir(dirname(__FILE__) .'/videos');
$rand=rand(2,count($videos) - 1);
$video = $videos[$rand];
?>
<style>

  .bg-video {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9999;
    background-color: #fff;
    overflow: hidden;
    background-position: center;
    background-repeat: no-repeat;

    background-image: url("<?php echo get_stylesheet_directory_uri();?>/img/o.gif");
  }

  .bg-video video {
    /* Make video to at least 100% wide and tall */
    min-width: 100%;
    min-height: 100%;

    /* Setting width & height to auto prevents the browser from stretching or squishing the video */
    width: auto;
    height: auto;

    /* Center the video */
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: none;
  }

  .bg-video video::-webkit-media-controls {
    display: none;
  }

  

  @media (max-width: 1024px) {
    .bg-video{
      background-size: 50% auto;
    }
    .bg-video video,
    .bg-video a {
      display: none !important;
    }
  }
</style>
<script>
  function hideVideo() {
    var video = document.getElementById('video');
    video.pause();
    video.src = null;
    document.querySelector('.bg-video').style.display = 'none';
    setTimeout(function () {
      if ($.fn.fullpage) {
        $.fn.fullpage.setAutoScrolling(true);
      }
    }, 1000);
  }   
</script>
<div class="bg-video">
  <video controls autoplay id="video" onended="hideVideo()" muted="muted">
    <source src="<?php echo get_stylesheet_directory_uri().'/videos/'.$video; ?>" type="video/mp4">
    Your browser does not support the video tag.
  </video>
  <a id="flecha" href="#inicio" onclick="hideVideo()"></a>
</div>