<?php
$page = get_page_by_path($slug);                  
$imagen = wp_get_attachment_image_src(get_post_thumbnail_id($page->ID),'full')[0];
$titulo = $page->post_title;
$subtitulo = get_field('page_subtitle',$page->ID);
include 'page_header.php';