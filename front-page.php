<?php get_header(); ?>
<style>
  .bg-video {
    
  }
  .overflow {
    overflow: hidden !important;
  }

  .bg-video video,
  .bg-video a {
    display: block;
  }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/fullpage.min.css">
<div id="fullpage" class="home">
  <div class="section section-1" data-anchor="inicio">
    <div class="bg-particulas">
      <div id="bg-verde"></div>
      <div id="bg-rojo"></div>
      <div id="bg-azul"></div>
    </div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-7">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/origami.png" class="img-fluid" alt="Origami">
        </div>
      </div>
    </div>
  </div>
  <div class="section identidad" data-anchor="identidad" id="identida">
    <div class="container-fluid">
      <div class="row justify-content-center no-gutters align-items-center">
        <div class="col-4 text-right">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/identidad.png" alt="Identidad">
          <p class="mt-3 pt-sm-3 pt-0">
            Marcas diseñadas para<br>
            ser únicas.
          </p>
        </div>
        <div class="col-6">
          <div class="embed-responsive">
            <img class="imagen" src="<?php echo get_stylesheet_directory_uri(); ?>/img/a.png?v=1" alt="A">
            <div class="embed-responsive-item parallax"
              style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/a.png?v=1');"></div>
          </div>
        </div>
      </div>
    </div>
    <a href="/work/#identidad" class="btn-section">
      +
    </a>
  </div>
  <div class="section campana" data-anchor="campana" id="campan">
    <div class="container-fluid">
      <div class="row justify-content-center no-gutters align-items-center">
        <div class="col-4 text-right">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/campana.png?v=1" alt="Campaña">
          <p class="mt-3 pt-sm-3 pt-0">
            Ideas que llegan al corazón de <br>
            las personas.
          </p>
        </div>
        <div class="col-3">
          <div class="embed-responsive">
            <img class="imagen" src="<?php echo get_stylesheet_directory_uri(); ?>/img/micro.png" alt="Micro">
            <div class="embed-responsive-item parallax-2"
              style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/micro.png');"></div>
          </div>
        </div>
      </div>
    </div>
    <a href="/work/#campana" class="btn-section">
      +
    </a>
  </div>
  <div class="section digital" data-anchor="digital" id="digita">
    <div class="container-fluid">
      <div class="row justify-content-center no-gutters align-items-center">
        <div class="col-3 text-right">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/digital.png" alt="Digital">
          <p class="mt-3 pt-sm-3 pt-0">
            Evolucionando las<br>
            marcas en el mundo<br>
            online.
          </p>
        </div>
        <div class="col-5">
          <div class="embed-responsive">
            <img class="imagen" src="<?php echo get_stylesheet_directory_uri(); ?>/img/satelite.png" alt="Satelite">
            <div class="embed-responsive-item parallax-3"
              style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/satelite.png');"></div>
          </div>
        </div>
      </div>
    </div>
    <a href="/work/#digital" class="btn-section">
      +
    </a>
  </div>
  <div class="section section-5 text-center" data-anchor="brifeanos">
    <h2>
      <strong>
        BRIFÉANOS
      </strong>
      <div style="line-height: 1.1em;">
        Y DEJEMOS MARCA
      </div>
    </h2>
    <div>
      <a href="/contact" class="btn-contacto">CONTACTO</a>
    </div>
    <div class="footer-fixed">
      <?php include_once('_footer.php'); ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/fullpage.js"></script>
<script src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.js"></script>
<script>
  $(function () {
    $('#fullpage').fullpage({
      //options here
      lockAnchors: false,
      autoScrolling: false,
      scrollHorizontally: true,
      scrollBar: true
    });
  });

  var bolas = function () {
    setTimeout(() => {
      particlesJS.load('bg-verde', '<?php echo get_stylesheet_directory_uri(); ?>/particles/logo/verde.json');
      particlesJS.load('bg-rojo', '<?php echo get_stylesheet_directory_uri(); ?>/particles/logo/rojo.json');
      particlesJS.load('bg-azul', '<?php echo get_stylesheet_directory_uri(); ?>/particles/logo/azul.json');
      particlesJS.load('identida', '<?php echo get_stylesheet_directory_uri(); ?>/particles/identidad.json');
      particlesJS.load('campan', '<?php echo get_stylesheet_directory_uri(); ?>/particles/campana.json');
      particlesJS.load('digita', '<?php echo get_stylesheet_directory_uri(); ?>/particles/digital.json');
    }, 1000);
  };

  $(function () {
    bolas();
    window.addEventListener("orientationchange", bolas);
  });
</script>