<?php get_header(); ?>
<style>
    .overflow {
        overflow: hidden !important;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/fullpage.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@5.3.0/css/swiper.min.css">
<?php 
if (have_posts()):
while (have_posts()): the_post();
?>
<div id="fullpage" class="about">
    <div class="section" id="about" data-anchor="nosotros">
        <div>
            <p>
                Lo divertido <br>
                de crear es llevar <br>
                las ideas <span>al límite</span>.
            </p>
        </div>
    </div>
    <div class="section equipo text-center" data-anchor="equipo">
        <div class="container">
            <h2>EQUIPO</h2>
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <p class="mt-4">Trabajamos en conjunto sin dividirnos por experiencia o
                        jerarquías. Pensamos que la diversidad de enfoques construye
                        mejores y grandes ideas.</p>
                </div>
            </div>
            <div class="mt-4">
                <div class="swiper-container">
                    <?php
                        $colaboradores = get_posts([
                            'post_type' => 'colaborador',
                            'post_status' => 'publish',
                            'numberposts' => -1,
                            'order'    => 'ASC'
                        ]);
                        ?>
                    <div class="swiper-wrapper">
                        <?php
                        foreach ($colaboradores as $colaborador):
                        $redes = array(                            
                            'behance'=>get_field('behance',$colaborador->ID),
                            'linkedin'=>get_field('linkedin',$colaborador->ID),
                            'instagram'=>get_field('instagram',$colaborador->ID)
                        );
                        ?>
                        <div class="swiper-slide">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-sm-5 col-10">

                                    <div class="embed-responsive embed-responsive-1by1">
                                        <div style="background-image: url(<?php echo get_the_post_thumbnail_url($colaborador->ID,'project-thumbnail'); ?>);"
                                            src="" alt="<?php echo $colaborador->post_title; ?>"
                                            class="embed-responsive-item rounded-circle"></div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <h3><?php echo $colaborador->post_title; ?></h3>
                                    <h4 class="mb-0"><?php echo get_field('cargo', $colaborador->ID); ?></h4>
                                    <p class="mt-0"><?php echo $colaborador->post_excerpt; ?></p>
                                    <div class="row justify-content-center align-items-center text-center">
                                        <?php foreach($redes as $fa => $red): ?>
                                        <?php if($red['usuario']) { ?>
                                        <div class="col-2">
                                            <a title="<?php echo $red['usuario']; ?>" class="icon-instagram"
                                                href="<?php echo $red['link']; ?>" target="_blank"><i
                                                    class="fa fa-<?php echo $fa; ?>" aria-hidden="true"></i></a>
                                        </div>
                                        <?php } ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="section clientes text-center" data-anchor="clientes">
        <?php 
    $clientes = get_posts([
      'post_type' => 'cliente',
      'post_status' => 'publish',
      'numberposts' => -1,
      'order'    => 'ASC'
    ]);
    ?>
        <div class="container">
            <div class="row no-gutters justify-content-center align-items-center" style="padding-bottom: 56px;">
                <div class="col-12">
                    <h2>CLIENTES</h2>
                </div>
                <?php foreach($clientes as $cliente): ?>
                <div class="col-sm-3 col-4 <?php echo $cliente->post_name; ?>">
                    <img src="<?php echo get_the_post_thumbnail_url($cliente->ID,'full'); ?>"
                        alt="<?php echo $cliente->post_title; ?>" class="img-fluid">
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="footer-absolute">
            <?php include_once('_footer.php'); ?>
        </div>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.8/fullpage.extensions.min.js"></script>
<script>
    $(function () {
        $(document).ready(function () {
            $('#fullpage').fullpage({
                lockAnchors: false,
                autoScrolling: false,
                scrollHorizontally: true,
                scrollBar: true,
                //paddingTop: document.querySelector('.navbar').offsetHeight / 2 + 'px'
            });
        });
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/swiper@5.3.0/js/swiper.min.js"></script>

<script>
    var swiper2 = new Swiper('.equipo .swiper-container', {
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
</script>